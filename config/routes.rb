Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root to: 'home#index'
  devise_for :users, skip: :registration
  resources :users
  resources :briefcases do
    resources :documents
  end
  resources :tec_leaders do
    collection { post :import}
  end
  resources :certifications
  resources :dependences do
    collection { post :import}
  end
  get '/import', to: 'tec_leaders#import_file'
  get '/import_dependence', to: 'dependences#import_file'
end
