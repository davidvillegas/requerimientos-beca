# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_28_013725) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "briefcases", force: :cascade do |t|
    t.integer "req_number"
    t.string "percent"
    t.date "date_apply"
    t.date "date_close"
    t.integer "rfc_number"
    t.string "details"
    t.integer "status"
    t.integer "kind"
    t.bigint "user_id"
    t.bigint "tec_leader_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["tec_leader_id"], name: "index_briefcases_on_tec_leader_id"
    t.index ["user_id"], name: "index_briefcases_on_user_id"
  end

  create_table "certifications", force: :cascade do |t|
    t.date "date"
    t.date "sent_date"
    t.string "details"
    t.integer "priority"
    t.integer "enviorment"
    t.integer "status"
    t.bigint "user_id"
    t.bigint "tec_leader_id"
    t.bigint "dependence_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dependence_id"], name: "index_certifications_on_dependence_id"
    t.index ["tec_leader_id"], name: "index_certifications_on_tec_leader_id"
    t.index ["user_id"], name: "index_certifications_on_user_id"
  end

  create_table "dependences", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "documents", force: :cascade do |t|
    t.string "name"
    t.bigint "briefcase_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["briefcase_id"], name: "index_documents_on_briefcase_id"
  end

  create_table "tec_leaders", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "name"
    t.integer "role"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  add_foreign_key "briefcases", "tec_leaders"
  add_foreign_key "briefcases", "users"
  add_foreign_key "certifications", "dependences"
  add_foreign_key "certifications", "tec_leaders"
  add_foreign_key "certifications", "users"
  add_foreign_key "documents", "briefcases"
end
