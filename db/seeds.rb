# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

User.create([{email: 'admin@admin.com', password: '4dm1nm0n1t0r', role: 'administrator', name: 'Administrador'},{email: 'test@test.com', password: '123456', role: 'regular', name: 'Usuario de Prueba'}])

TecLeader.create([{name: 'Usuario de Prueba'}])

Dependence.create([{name: 'DPTO. MONITOR DE OP DE SERV Y SOL TI'}, {name: 'DIV. OPERACION DE SERV Y SOLUCIONES TI'}, {name: 'DPTO. ASEGURAMIENTO DE LA CALIDAD'}])

# ReqStatus.create([{status: 'Activo'},{status: 'Suspendido'},{status: 'Detenido'},{status: 'Cancelado'},{status: 'Completado'}])

# ReqKind.create([{kind: 'Operativo'},{kind: 'Regulatorio'},{kind: 'Técnico'}])

