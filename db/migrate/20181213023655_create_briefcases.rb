class CreateBriefcases < ActiveRecord::Migration[5.2]
  def change
    create_table :briefcases do |t|
      t.integer :req_number
      t.string :percent
      t.date :date_apply
      t.date :date_close
      t.integer :rfc_number
      t.string :details
      t.integer :status
      t.integer :kind
      t.references :user, foreign_key: true
      t.references :tec_leader, foreign_key: true

      t.timestamps
    end
  end
end
