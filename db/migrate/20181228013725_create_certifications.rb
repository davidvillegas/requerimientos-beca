class CreateCertifications < ActiveRecord::Migration[5.2]
  def change
    create_table :certifications do |t|
      t.date :date
      t.date :sent_date
      t.string :details
      t.integer :priority
      t.integer :enviorment
      t.integer :status
      t.references :user, foreign_key: true
      t.references :tec_leader, foreign_key: true
      t.references :dependence, foreign_key: true

      t.timestamps
    end
  end
end
