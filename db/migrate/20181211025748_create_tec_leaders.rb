class CreateTecLeaders < ActiveRecord::Migration[5.2]
  def change
    create_table :tec_leaders do |t|
      t.string :name

      t.timestamps
    end
  end
end
