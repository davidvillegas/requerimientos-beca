class DependencesController < ApplicationController
  before_action :authenticate_user!
  before_action :is_admin?
  before_action :redirect_if_not_admin
  before_action :set_dependence, only: [:edit, :update, :destroy]

  def index
    @dependences = Dependence.all
  end

  def new
    @dependence = Dependence.new
  end

  def create
    @dependence = Dependence.create(dependence_params)
    if @dependence.save
      redirect_to dependences_path, notice: "El Departamento se registró satisfactoriamente"
    else
      render :new
    end
  end

  def show

  end

  def edit

  end

  def update
    if @dependence.update(dependence_params)
      redirect_to dependences_path, notice: "El Departamento se actualizó correctamente"
    else
      render :edit
    end
  end

  def destroy
    if @dependence.destroy
      redirect_to dependences_path, notice: "El Departamento ha sido eliminado"
    end
  end

  def import_file

  end

  def import
    if Dependence.import(params[:file])
      redirect_to dependences_path, notice: "¡ Lista de Líderes agregada correctamente !"
    else
      redirect_to root_path, alert: "¡ERROR! La lista no pudo ser agregada correctamente. Revise el formato del archivo"
    end
  end

  private

  def dependence_params
    params.require(:dependence).permit(:name)
  end

  def set_dependence
    @dependence = Dependence.find(params[:id])
  end

  def is_admin?
    @is_admin = current_user.role == 'administrator'
  end

  def redirect_if_not_admin
    unless @is_admin then redirect_to root_path, alert: "No posees los permisos necesarios" end
  end

end
