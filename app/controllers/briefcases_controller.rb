class BriefcasesController < ApplicationController
  before_action :authenticate_user!
  before_action :set_briefcase, only: [:show, :edit, :update, :destroy]
  before_action :set_user_sec, only: [:new, :edit, :create]
  before_action :set_leader_tec, only: [:new, :edit, :create]

  def index
    @briefcases = Briefcase.all
  end

  def new
    @briefcase = Briefcase.new
    @document = @briefcase.documents.build
  end

  def create
    @briefcase = Briefcase.create(briefcase_params)
    if @briefcase.save
      redirect_to briefcases_path, notice: "La Certificación se registró satisfactoriamente"
    else
      render :new
    end
  end

  def show

  end

  def edit

  end

  def update
    if @briefcase.update(briefcase_params)
      redirect_to briefcases_path, notice: "La Certificación se actualizó correctamente"
    else
      render :edit
    end
  end

  def destroy
    if @briefcase.destroy
      redirect_to briefcases_path, notice: "La Certificación ha sido eliminada"
    end
  end

  private

  def set_briefcase
    @briefcase = Briefcase.find(params[:id])
  end

  def briefcase_params
    params.require(:briefcase).permit(:req_number, :percent, :date_apply, :date_close, :rfc_number, :details, :status, :kind, :user_id, :tec_leader_id, documents_attributes: [:name, :attachment])
  end

  def set_user_sec
    @users = User.all.collect { |user| [user.name, user.id] }
  end

  def set_leader_tec
    @leaders = TecLeader.all.collect { |leader| [leader.name, leader.id] }
  end
end
