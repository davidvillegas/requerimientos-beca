class UsersController < ApplicationController
  before_action :authenticate_user!
  before_action :is_admin?
  before_action :redirect_if_not_admin, except: [:edit, :update]
  before_action :check_current_user, only: [:edit, :update, :show]

  def index
    @users = User.where.not(:id => current_user.id)
    # @users = User.all
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if @user.save
      flash[:notice] = "Se ha creado el Usuario satisfactoriamente"
      redirect_to users_path
    else
      render :action => 'new'
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update(user_params)
      flash[:notice] = "Se ha actualizado el Usuario satisfactoriamente"
      if @is_admin
        if params[:id].to_i != current_user.id
          redirect_to users_path
        else
          bypass_sign_in(@user)
          redirect_to root_path
        end
      else
        bypass_sign_in(@user)
        redirect_to root_path
      end
    else
      render :action => 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    if @user.destroy
      flash[:notice] = "El Usuario ha sido eliminado"
      redirect_to users_path
    end
  end

  private

  def user_params
    params.require(:user).permit(:email, :password, :password_confirmation, :name, :role)
  end

  def is_admin?
    @is_admin = current_user.role == 'administrator'
  end

  def redirect_if_not_admin
    unless @is_admin then redirect_to root_path, alert: "No posees los permisos necesarios" end
  end

  def check_current_user
    if current_user.present?
      if current_user.role == 'regular'
        if params[:id].to_i != current_user.id
          redirect_back fallback_location: root_path, alert: "Estas intentando acceder a una cuenta que no te pertenece"
        end
      end
    else
      redirect_back fallback_location: root_path, alert: "Tu Sesión ha expirado"
    end
	end
end
