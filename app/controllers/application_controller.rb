class ApplicationController < ActionController::Base
	# before_action :authenticate
	# include Error::ErrorHandler
  rescue_from ActiveRecord::RecordNotFound, with: :handle_exception
	# rescue_from Exception, :with => :handle_exception_2
	private

		def handle_exception
			render html: 'El registro a que haces referencia, no extiste', status: :not_found
		end

		def handle_exception_2
			render html: 'Página No Encontrada', status: :unauthorized
		end

		# def authenticate
		# 	if controller_name != 'home'
		# 		:authenticate_user!
		# 	end
		# end

	protected

end
