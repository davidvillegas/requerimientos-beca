class CertificationsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_certification, only: [:show, :edit, :update, :destroy]
  before_action :set_user_sec, only: [:new, :edit, :create]
  before_action :set_leader_tec, only: [:new, :edit, :create]
  before_action :set_dependece, only: [:new, :edit, :create]

  def index
    @certifications = Certification.all
  end

  def new
    @certification = Certification.new
  end

  def create
    @certification = Certification.new(certification_params)
    if @certification.save
      redirect_to certifications_path, notice: "La Certificación se registró satisfactoriamente"
    else
      render :new
    end
  end

  def show

  end

  def edit

  end

  def update
    if @certification.update(certification_params)
      redirect_to certifications_path, notice: "La Certificación se actualizó satisfactoriamente"
    else
      render :edit
    end
  end

  def destroy
    if @certification.destroy
      redirect_to certifications_path, notice: "El Usuario ha sido eliminado"
    end
  end

  private

  def set_certification
    @certification = Certification.find(params[:id])
  end

  def certification_params
    params.require(:certification).permit(:date, :sent_date, :details, :priority, :enviorment, :status, :user_id, :tec_leader_id, :dependence_id, :document)
  end

  def set_user_sec
    @users = User.all.collect { |user| [user.name, user.id] }
  end

  def set_leader_tec
    @leaders = TecLeader.all.collect { |leader| [leader.name, leader.id] }
  end

  def set_dependece
    @dependences = Dependence.all.collect { |dependence| [dependence.name, dependence.id] }
  end
end
