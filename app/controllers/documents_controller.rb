class DocumentsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_briefcase
  before_action :set_document, only: [:show, :edit, :update, :destroy]

  def index
    @documents = @briefcase.documents.where(briefcase_id: @briefcase)
  end

  def new
    @document = Document.new
  end

  def create
    @document = @briefcase.documents.build(document_params)
    if @document.save
      redirect_to briefcase_documents_path, notice: "Documento registrado exitosamente"
    else
      render :new
    end
  end

  def show

  end

  def edit

  end

  def update
    if @document.update(document_params)
      redirect_to briefcase_documents_path, notice: "Documento actualizado exitosamente"
    else
      render :edit
    end
  end

  def destroy
    if @document.destroy
      redirect_to briefcase_documents_path, notice: "Documento eliminado exitosamente"
    end
  end

  private

  def document_params
    params.require(:document).permit(:name, :attachment)
  end

  def set_briefcase
    @briefcase = Briefcase.find(params[:briefcase_id])
  end

  def set_document
     @document = Document.find(params[:id])
  end

end
