class TecLeadersController < ApplicationController
  before_action :authenticate_user!
  before_action :is_admin?
  before_action :redirect_if_not_admin

  def import_file
  end

  def index
    @tecleaders = TecLeader.all
  end

  def new
    @tecleader = TecLeader.new
  end

  def create
    @tecleader = TecLeader.new(tecleader_params)
    if @tecleader.save
      flash[:notice] = "Se ha creado el registro satisfactoriamente"
      redirect_to tec_leaders_path
    else
      render :action => 'new'
    end
  end

  def edit
    @tecleader = TecLeader.find(params[:id])
  end

  def update
    @tecleader = TecLeader.find(params[:id])
    if @tecleader.update(tecleader_params)
      flash[:notice] = "Se ha actualizado el registro satisfactoriamente"
      redirect_to tec_leaders_path
    else
      render :action => 'edit'
    end
  end

  def destroy
    @tecleader = TecLeader.find(params[:id])
    if @tecleader.destroy
      flash[:notice] = "¡ El registro seleccionado, ha sido eliminado exitosamente !"
      redirect_to tec_leaders_path
    else
      redirect_to tec_leaders_path, alert: '¡ No fue posible eliminar el registro seleccionado !'
    end
  end

  def import
    if TecLeader.import(params[:file])
      redirect_to tec_leaders_path, notice: "¡ Lista de Líderes agregada correctamente !"
    else
      redirect_to root_path, alert: "¡ERROR! La lista no pudo ser agregada correctamente. Revise el formato del archivo"
    end
  end

  private

  def tecleader_params
    params.require(:tec_leader).permit(:name)
  end

  def is_admin?
    @is_admin = current_user.role == 'administrator'
  end

  def redirect_if_not_admin
    unless @is_admin then redirect_to root_path, alert: "No posees los permisos necesarios" end
  end

end
