class ApplicationMailer < ActionMailer::Base
  default from: 'requerimientos@bancoexterior.com'
  layout 'mailer'
end
