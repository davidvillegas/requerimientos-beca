class Dependence < ApplicationRecord
  require 'csv'
  has_many :certifications

  validates :name, presence: true

  def self.import(file)
    if file.path.split('.').last.to_s.downcase == 'csv'
      self.check_file(file)
    else
      return false
    end
  end

  private

  def self.check_file(file)
    CSV.foreach(file.path, headers: true, encoding: 'iso-8859-1:utf-8', col_sep: ";") do |row|
      if (row.headers[0] != 'name')
        return false
      else
        if row.length > 1
          return false
        else
          if row[0].blank?
            return false
          else
            Dependence.create! row.to_hash
          end
        end
      end
    end
    return true
  end

end
