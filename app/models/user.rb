class User < ApplicationRecord
  enum role: [:regular, :administrator]

  validates :name, presence: true
  after_initialize :set_default_role, :if => :new_record?

  has_many :briefcases
  has_many :certifications

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def set_default_role
    self.role ||= :regular
  end

end
