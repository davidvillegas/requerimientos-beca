class Briefcase < ApplicationRecord
  enum status: [:activo, :suspendido, :detenido, :cancelado, :completado]
  enum kind: [:regulatorio, :operativo, :tecnico]

  validates :req_number, uniqueness: true

  belongs_to :user
  belongs_to :tec_leader
  has_many :documents
  accepts_nested_attributes_for :documents, reject_if: :all_blank
end
