class Document < ApplicationRecord
  belongs_to :briefcase
  has_one_attached :attachment
end
