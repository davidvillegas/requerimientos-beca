class Certification < ApplicationRecord
  enum priority: [:normal, :urgente]
  enum enviorment: [:desarrollo, :calidad, :producción]
  enum status: [:pendiente, :enviada]
  belongs_to :user
  belongs_to :tec_leader
  belongs_to :dependence
  has_one_attached :document
  validate :correct_file_type

  private

  def correct_file_type
    if document.attached? && !document.content_type.in?(%w(application/pdf))
      errors.add(:document, 'Seleccionar solo archivos con la extensión .pdf')
    end
  end

end
